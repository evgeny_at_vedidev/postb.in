// --------------------------------------------------------------------------------------------------------------------

'use strict'

// local
var pkg = require('../../package.json')
var env = require('../env.js')
var db = require('../db.js')

// --------------------------------------------------------------------------------------------------------------------

module.exports = function (app) {
  app.get('/', function (req, res, next) {
    res.render('index', {
      title: pkg.title
    })
  })

  app.post('/b/', function (req, res, next) {
    // since someone is posting here, let's create a bin and redirect to it
    db.createBin(function (err, bin) {
      if (err) return next(err)
      res.redirect('/b/' + bin.binId)
    })
  })

  var pages = [
    '/',
    '/api/'
  ]
  var sitemap = pages.map(function (page) {
    return env.baseUrl + page + '\n'
  }).join('')

  app.get('/sitemap.txt', function (req, res) {
    res.set('Content-Type', 'text/plain')
    res.send(sitemap)
  })
}

// --------------------------------------------------------------------------------------------------------------------
